package com.pm.spark

//import org.apache.spark.sql.SparkSession
import org.apache.spark.SparkContext
import org.apache.spark.SparkConf

object SparkTest {
  def main(args: Array[String]): Unit = {
    //val spark = SparkSession.builder().appName("SparkTest").getOrCreate()
    val conf = new SparkConf().setAppName("sparkTest").setMaster("local")
    val sc = new SparkContext(conf)
    println("Hello Spark")
  }
}
